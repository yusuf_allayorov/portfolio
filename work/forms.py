from django.forms import ModelForm
from .models import Work

class AddWork(ModelForm):
    class Meta:
        model = Work
        fields = ['img', 'name', 'title']
    