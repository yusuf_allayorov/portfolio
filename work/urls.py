from django.urls import path
from .views import work, addwork
app_name = 'work'
urlpatterns = [
    path('', work, name = 'work'),
     path('addwork/', addwork, name = 'addwork'),
]