from django.shortcuts import render, get_object_or_404,redirect
from .models import Work
from .forms import AddWork
# Create your views here.
def work(request):
    obj = Work.objects.all()
    return render(request, 'work.html', {'obj':obj})


def addwork(request):
    form = AddWork()
    if request.method == 'POST':
        form = AddWork(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('work:work')
    return render(request,'addwork.html',{'addwork': form})