from django.db import models

# Create your models here.
class Work(models.Model):
    img = models.ImageField(upload_to = "work/")
    name = models.CharField(max_length=40)
    title = models.CharField(max_length=30)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name