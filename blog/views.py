from django.shortcuts import render, get_object_or_404,redirect
from .models import Blog
from .forms import AddBlog
# Create your views here.
def blog(request):
    yus = request.GET.get('yus')
    print(yus)
    obj = Blog.objects.all()
    if request.GET:
        obj = Blog.objects.filter(title__contains=yus)
    return render(request,'blog.html', {'obj': obj})

def detiel(request,id):
    blog = get_object_or_404(Blog,id=id)
    blog.blog_count += 1
    blog.save()
    return render(request, 'detiel.html', {'blog': blog,})

def addblog(request):
    form = AddBlog()
    if request.method == 'POST':
        form = AddBlog(request.POST, request.FILES)
        if form.is_valid():
            
            form.save()
            return redirect('blog:blog')
    return render(request, 'addblog.html', {'addblog': form})

def updateblog(request,id):
    blog = get_object_or_404(Blog, id=id)
    if request.method == 'POST':
        form = AddBlog(request.POST, instance=blog)
        if form.is_valid():
            form.save()
            return redirect('blog:blog')
    else:
        form = AddBlog(instance=blog)
    return render(request,'updateblog.html', {'form': form, 'blog':blog})

def deleteblog(request,id):
    blog = get_object_or_404(Blog, id=id)
    form = AddBlog(request.POST, instance=blog)
    if request.method == 'POST':
        blog.delete()
        return redirect('blog:blog')
    return render(request,'deleteblog.html', {'form': form, 'blog': blog})
