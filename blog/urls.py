from django.urls import path
from .views import blog,detiel, addblog, updateblog,deleteblog
app_name = 'blog'
urlpatterns = [
    path('', blog , name = 'blog'),
    path('detiel/<int:id>', detiel, name = 'detiel'),
    path('addblog/', addblog, name = 'addblog'),
    path('updateblog/<int:id>', updateblog, name = 'updateblog'),
     path('deleteblog/<int:id>', deleteblog, name = 'deleteblog'),
]