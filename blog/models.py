from django.db import models

# Create your models here.

class Blog(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField()
    img = models.ImageField(upload_to = "blog/")
    imgAuthor = models.ImageField(upload_to = "blog/")
    author = models.CharField(max_length=100, blank=True, null=True)
    date = models.DateTimeField(auto_now_add=True)
    blog_count = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.title} {self.author}'