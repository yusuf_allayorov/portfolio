from django.forms import ModelForm
from .models import Blog

class AddBlog(ModelForm):
    class Meta:
        model = Blog
        fields = ['title', 'body', 'img', 'imgAuthor', 'author']
    
    # def __init__(self, *args, **kwargs):
    #     super(AddBlog,self).__init__(*args, **kwargs)
    #     self.