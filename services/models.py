from django.db import models

# Create your models here.
class Services(models.Model):
    work = models.CharField(max_length=20)
    year = models.CharField(max_length=2)
    client = models.CharField(max_length=5)
    won = models.CharField(max_length=3)

    def __str__(self):
        return f'services'