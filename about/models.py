from django.db import models

# Create your models here.
class About(models.Model):
    email = models.EmailField()
    phone = models.CharField(max_length=15)
    img = models.ImageField(upload_to = "about/")
    html = models.CharField(max_length=15)
    css = models.CharField(max_length=15)
    py = models.CharField(max_length=15)
    js = models.CharField(max_length=15)

    def __str__(self):
        return self.email