from django.shortcuts import render, redirect, get_object_or_404
from .models import About
from .forms import AboutForm
# Create your views here.
def about(request):
    about = About.objects.all()
    return render(request, 'about.html', {'about': about})